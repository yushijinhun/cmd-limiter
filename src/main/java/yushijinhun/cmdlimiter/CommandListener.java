package yushijinhun.cmdlimiter;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CommandBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import yushijinhun.cmdlimiter.process.LimitHandler;

public class CommandListener implements Listener {

	private LimitHandler handler;
	private Runnable ticker = new Runnable() {

		@Override
		public void run() {
			handler.tick();
		}
	};

	public CommandListener(LimitHandler handler) {
		this.handler = handler;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockRedstone(BlockRedstoneEvent event) {
		Block block = event.getBlock();
		if (event.getNewCurrent() != 0 && block.getType() == Material.COMMAND && block.getState() instanceof CommandBlock) {
			CommandBlock cmdblock = (CommandBlock) block.getState();
			String rawcmd = cmdblock.getCommand();
			if (rawcmd.startsWith("/")) {
				rawcmd = rawcmd.substring(1);
			}
			String splited[] = rawcmd.split(" ", 2);
			if (splited.length > 1) {
				String cmd = splited[0].trim();
				if (cmd.startsWith("minecraft:")) {
					cmd = cmd.substring("minecraft:".length());
				}
				if (!cmd.isEmpty()) {
					Location location = block.getLocation();
					boolean allow = handler.onCommandExecute(cmd, location);
					if (!allow) {
						event.setNewCurrent(0);
					}
				}
			}
		}
	}

	public Runnable getTicker() {
		return ticker;
	}

}
