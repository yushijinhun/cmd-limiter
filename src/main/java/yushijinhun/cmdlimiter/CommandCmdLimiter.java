package yushijinhun.cmdlimiter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import yushijinhun.cmdlimiter.process.LimitHandler;
import yushijinhun.cmdlimiter.process.LimitRule;

public class CommandCmdLimiter implements CommandExecutor, TabCompleter {

	private final List<String> subcmds = Arrays.asList("show", "set", "unset", "save", "load");

	private LimitHandler handler;

	public CommandCmdLimiter(LimitHandler handler) {
		this.handler = handler;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdline, String args[]) {
		if (args.length == 0) {
			return false;
		}

		switch (args[0]) {
			case "show":
				if (args.length == 2) {
					String objcmd = args[1];
					if (objcmd.equalsIgnoreCase("all"))
						sender.sendMessage(handler.getRules().getRules().toString());
					else if (objcmd.equalsIgnoreCase("default"))
						sender.sendMessage(handler.getRules().getDefaultRule().toString());
					else
						sender.sendMessage(handler.getRules().getRules().get(objcmd.toLowerCase()).toString());
					return true;
				} else {
					return false;
				}

			case "set":
				if (args.length == 4) {
					String objcmd = args[1];
					int maxper = Integer.parseInt(args[2]);
					int maxglo = Integer.parseInt(args[3]);
					LimitRule rule = new LimitRule(maxper, maxglo);
					if (objcmd.equalsIgnoreCase("default"))
						handler.getRules().setDefaultRule(rule);
					else
						handler.getRules().addRule(objcmd, rule);
					sender.sendMessage("rule has set");
					return true;
				} else {
					return false;
				}

			case "unset":
				if (args.length == 2) {
					String objcmd = args[1];
					if (handler.getRules().removeRule(objcmd)) {
						sender.sendMessage("rule has removed");
					} else {
						sender.sendMessage(ChatColor.RED + "no such rule");
					}
					return true;
				} else {
					return false;
				}

			case "save":
				if (args.length == 1) {
					if (handler.saveRules()) {
						sender.sendMessage("rules has saved");
					} else {
						sender.sendMessage(ChatColor.RED + "failed to save rules, see console logs");
					}
					return true;
				} else {
					return false;
				}

			case "load":
				if (args.length == 1) {
					if (handler.loadRules()) {
						sender.sendMessage("rules has reloaded");
					} else {
						sender.sendMessage(ChatColor.RED + "failed to reload rules, see console logs");
					}
					return true;
				} else {
					return false;
				}

			default:
				return false;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String cmdline, String args[]) {
		if (args.length == 1) {
			List<String> list = new ArrayList<>();
			String arg1 = args[0];
			for (String subcmd : subcmds) {
				if (subcmd.startsWith(arg1)) {
					list.add(subcmd);
				}
			}
			return list;
		}
		return null;
	}

}
