package yushijinhun.cmdlimiter.process;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentSkipListMap;

public class LimitRuleSet {

	private int limitUnit = 20;
	private LimitRule defaultRule = new LimitRule(-1, -1);
	private Map<String, LimitRule> rules = new ConcurrentSkipListMap<>();

	public LimitRule getDefaultRule() {
		return defaultRule;
	}

	public void setDefaultRule(LimitRule defaultRule) {
		Objects.requireNonNull(defaultRule);
		this.defaultRule = defaultRule;
	}

	public Map<String, LimitRule> getRules() {
		ensureConcurrentMap();
		return rules;
	}

	public void setRules(Map<String, LimitRule> rules) {
		Objects.requireNonNull(rules);
		ensureConcurrentMap();
		this.rules = rules;
	}

	public LimitRule lookupRule(String command) {
		Objects.requireNonNull(command);
		command = command.toLowerCase();
		LimitRule rule = rules.get(command);
		if (rule == null) {
			return defaultRule;
		}
		return rule;
	}

	public void addRule(String command, LimitRule rule) {
		Objects.requireNonNull(command);
		Objects.requireNonNull(rule);
		command = command.toLowerCase();
		rules.put(command, rule);
	}

	public boolean removeRule(String command) {
		Objects.requireNonNull(command);
		command = command.toLowerCase();
		return rules.remove(command) != null;
	}

	public int getLimitUnit() {
		return limitUnit;
	}

	public void setLimitUnit(int limitUnit) {
		this.limitUnit = limitUnit;
	}

	@Override
	public int hashCode() {
		return Objects.hash(defaultRule, rules);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof LimitRuleSet) {
			LimitRuleSet another = (LimitRuleSet) obj;
			return defaultRule.equals(another.defaultRule) && rules.equals(another.rules);
		}
		return false;
	}

	@Override
	public String toString() {
		return "[default=" + defaultRule + ", rules=" + rules + "]";
	}

	private void ensureConcurrentMap() {
		if (!(rules instanceof ConcurrentSkipListMap)) {
			// convert it to a concurrent map
			rules = new ConcurrentSkipListMap<>(rules);
		}
	}

}
