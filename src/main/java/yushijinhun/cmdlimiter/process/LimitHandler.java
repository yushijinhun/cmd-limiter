package yushijinhun.cmdlimiter.process;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.DumperOptions.LineBreak;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.representer.Representer;

public class LimitHandler {

	private Logger logger;
	private File ruleDatabase;

	private volatile long ticks = 0;
	private volatile LimitRuleSet rules = new LimitRuleSet();
	private Map<Location, Map<String, AtomicInteger>> blockExecutes = new ConcurrentHashMap<>();
	private Map<String, AtomicInteger> globalExecutes = new ConcurrentHashMap<>();

	public LimitHandler(Logger logger, File ruleDatabase) {
		this.logger = logger;
		this.ruleDatabase = ruleDatabase;
	}

	public LimitRuleSet getRules() {
		return rules;
	}

	/**
	 * @param command the command
	 * @param location command block location
	 * @return true if allow, false if deny
	 */
	public boolean onCommandExecute(String command, Location location) {
		if (logger.isLoggable(Level.FINEST)) {
			logger.finest("onCommandExecute: '" + command + "' at " + location);
		}

		LimitRule rule = rules.lookupRule(command);
		if (rule.getLimitGlobal() == 0 || rule.getLimitPerCommandBlock() == 0) {
			return false;
		}

		if (rule.getLimitGlobal() > 0) {
			AtomicInteger store = globalExecutes.get(command);
			if (store == null) {
				store = new AtomicInteger(1);
				globalExecutes.put(command, store);
			} else {
				if (store.get() < rule.getLimitGlobal()) {
					store.incrementAndGet();
				} else {
					return false;
				}
			}
		}

		if (rule.getLimitPerCommandBlock() > 0 && location != null) {
			Map<String, AtomicInteger> mapping = blockExecutes.get(location);
			if (mapping == null) {
				mapping = new ConcurrentHashMap<>();
				blockExecutes.put(location, mapping);
			}
			AtomicInteger store = mapping.get(command);
			if (store == null) {
				store = new AtomicInteger(1);
				mapping.put(command, store);
			} else {
				if (store.get() < rule.getLimitPerCommandBlock()) {
					store.incrementAndGet();
				} else {
					return false;
				}
			}
		}

		return true;
	}

	public void tick() {
		ticks++;
		if (ticks % rules.getLimitUnit() == 0) {
			resetCount();
		}
		if (ticks % rules.getLimitUnit() * 60 == 0) {
			cleanup();
		}
	}

	private void resetCount() {
		for (AtomicInteger store : globalExecutes.values()) {
			store.set(0);
		}
		for (Map<String, AtomicInteger> mapping : blockExecutes.values()) {
			for (AtomicInteger store : mapping.values()) {
				store.set(0);
			}
		}
	}

	private void cleanup() {
		logger.fine("cleanup commandblock limit cahces");

		globalExecutes.clear();
		blockExecutes.clear();
	}

	public boolean loadRules() {
		try (Reader reader = new InputStreamReader(new BufferedInputStream(new FileInputStream(ruleDatabase)), "UTF-8")) {
			rules = createYaml().loadAs(reader, LimitRuleSet.class);
			logger.info("Loaded commandblock limit rules");
			return true;
		} catch (IOException | YAMLException e) {
			logger.warning("Failed to load commandblock limit rules: " + e);
			e.printStackTrace();
			return false;
		}
	}

	public boolean saveRules() {
		try (Writer writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(ruleDatabase)), "UTF-8")) {
			createYaml().dump(rules, writer);
			logger.info("Saved commandblock limit rules");
			return true;
		} catch (IOException | YAMLException e) {
			logger.warning("Failed to save commandblock limit rules: " + e);
			e.printStackTrace();
			return false;
		}
	}

	private Yaml createYaml() {
		DumperOptions options = new DumperOptions();
		options.setLineBreak(LineBreak.UNIX);
		return new Yaml(new Constructor() {

			@Override
			protected Class<?> getClassForName(String name) throws ClassNotFoundException {
				try {
					return Class.forName(name);
				} catch (ClassNotFoundException e) {
					return super.getClassForName(name);
				}
			}

		}, new Representer(), options);
	}

}
