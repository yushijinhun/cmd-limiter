package yushijinhun.cmdlimiter.process;

import java.util.Objects;

public class LimitRule {

	private int limitPerCommandBlock = -1;
	private int limitGlobal = -1;
	
	public LimitRule() {
	}

	public LimitRule(int limitPerCommandBlock, int limitGlobal) {
		this.limitPerCommandBlock = limitPerCommandBlock;
		this.limitGlobal = limitGlobal;
	}

	public int getLimitPerCommandBlock() {
		return limitPerCommandBlock;
	}

	public void setLimitPerCommandBlock(int limitPerCommandBlock) {
		this.limitPerCommandBlock = limitPerCommandBlock;
	}

	public int getLimitGlobal() {
		return limitGlobal;
	}

	public void setLimitGlobal(int limitGlobal) {
		this.limitGlobal = limitGlobal;
	}

	@Override
	public int hashCode() {
		return Objects.hash(limitPerCommandBlock, limitGlobal);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof LimitRule) {
			LimitRule another = (LimitRule) obj;
			return limitPerCommandBlock == another.limitPerCommandBlock && limitGlobal == another.limitGlobal;
		}
		return false;
	}

	@Override
	public String toString() {
		return "[perCommandblock=" + limitPerCommandBlock + ", global=" + limitGlobal + "]";
	}

}
