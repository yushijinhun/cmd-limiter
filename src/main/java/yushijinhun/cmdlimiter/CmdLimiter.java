package yushijinhun.cmdlimiter;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import yushijinhun.cmdlimiter.process.LimitHandler;

public class CmdLimiter extends JavaPlugin {

	// configures
	private Level logLevel = Level.INFO;

	private Logger logger;
	private CommandListener listener;
	private LimitHandler limitHandler;

	@Override
	public void onEnable() {
		logger = getLogger() == null ? Logger.getLogger("yushijinhun.cmdlimiter") : getLogger();
		doLoadConfig(getConfig());
		logger.setLevel(logLevel);
		doSaveConfig(getConfig());
		saveConfig();

		limitHandler = new LimitHandler(logger, new File(getDataFolder(), "rules.yml"));
		limitHandler.loadRules();
		listener = new CommandListener(limitHandler);
		getServer().getPluginManager().registerEvents(listener, this);
		getServer().getScheduler().runTaskTimer(this, listener.getTicker(), 0, 1);

		PluginCommand cmdlimit = getCommand("cmdlimit");
		CommandCmdLimiter cmdlimitExecutor = new CommandCmdLimiter(limitHandler);
		cmdlimit.setExecutor(cmdlimitExecutor);
		cmdlimit.setTabCompleter(cmdlimitExecutor);
	}

	@Override
	public void onDisable() {
		limitHandler.saveRules();
	}

	private void doLoadConfig(FileConfiguration config) {
		logLevel = Level.parse(config.getString("log_level", "INFO"));
	}

	private void doSaveConfig(FileConfiguration config) {
		config.set("log_level", logLevel.toString());
	}

}
